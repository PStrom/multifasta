import re


class Header(object):
    def __init__(self, line: str):
        self.line = '>'+line

    def to_string(self):
        return self.line


class Sequence(object):
    def __init__(self, line: str):
        self.line = line.replace('-', '')

    def to_string(self):
        return self.line


class Gene(object):
    def __init__(self, header: Header, sequence: Sequence):
        self.header = header
        self.sequence = sequence

    @staticmethod
    def from_line(line: str):
        header_string = ""
        sequence_string = ""
        m = re.match("^(.*?)([ATCG-]{16,})", line)
        if m:
            header_string = m.group(1).strip()
            sequence_string = m.group(2).strip()
        return Gene(Header(header_string), Sequence(sequence_string))
