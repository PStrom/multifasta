import os

from source.gene import Gene
import configuration


# The main program.
class Main(object):
    @staticmethod
    def run():
        filenames = Main.read_files()

        for filename in filenames:
            print('Processing : ' + filename)
            genes = Main.read_genes_from_file(filename)
            Main.generate_multifasta_file(filename, genes)

    @staticmethod
    def read_files():
        filenames = []
        for root, directories, files in os.walk(configuration.INPUT_FOLDER):
            for file in files:
                if file.endswith(configuration.FILE_INPUT_EXTENTION_FILTER):
                    filenames.append(file)
        return filenames

    @staticmethod
    def read_genes_from_file(filename):
        with open(configuration.INPUT_FOLDER + filename) as in_file:
            genes = []
            for line in in_file:
                gene = Gene.from_line(line)
                genes.append(gene)
        return genes

    @staticmethod
    def generate_multifasta_file(filename, genes):
        writefilename = configuration.OUTPUT_FOLDER + configuration.OUTPUT_FILE_PREFIX + filename
        with open(writefilename, 'w') as out_file:
            for gene in genes:
                out_file.write(gene.header.to_string() + "\n")
                out_file.write(gene.sequence.to_string() + "\n")


Main.run()
input('Press any key to close')
