# The folder where you put the files to be read in (with the old format)
INPUT_FOLDER = 'input/'
# The program will only read files that end with this format.
FILE_INPUT_EXTENTION_FILTER = '.txt'

# The folder where the files will end up in with the new format.
OUTPUT_FOLDER = 'output/'
# This is a prefix that gets set on the files in the output folder, to easier separate them from the
# files in the old format. It's just prepended to the old filename.
OUTPUT_FILE_PREFIX = 'MULTIFASTA_'
