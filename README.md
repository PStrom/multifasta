﻿# Multifasta

A simple script to transform gene sequence reading into multifasta format.


## Prerequisites
Requires python3.5 or higher to be installed, and on the path

## Installation
Place the folder structure into a directory of your liking, no installation required.
Make sure there are two folders, `input` and `output` in the multifasta
directory

Inside the configuration.py file there are some options to customise the input and output.


## How to use
Put the text files with the old format sequences in the input folder
Run the code, by typing the following code in the terminal:

```
py multifasta.py
```

The output folder will contain the newly formatted files.

## Author
Peder Strömberg <pederstromberg@gmail.com>

## Licence
MIT

Copyright 2017 Peder Strömberg

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
